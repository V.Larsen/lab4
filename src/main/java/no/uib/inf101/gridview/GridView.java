package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;


public class GridView extends JPanel {
  IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private static final double gridMargin = 30;

  public GridView(IColorGrid colorGrid) {
    this.colorGrid = colorGrid;
    this.setPreferredSize(new Dimension(400, 300));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }

  private void drawGrid(Graphics2D graphics2D) {
    Rectangle2D grey = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth() - 2 * OUTERMARGIN, getHeight() - 2 * OUTERMARGIN);
    graphics2D.setColor(MARGINCOLOR);
    graphics2D.fill(grey);
    CellPositionToPixelConverter pixels = new CellPositionToPixelConverter(grey, colorGrid, gridMargin);
    drawCells(graphics2D, colorGrid, pixels);
  }
  private static void drawCells(Graphics2D graphics2D, CellColorCollection cellColorCollection, CellPositionToPixelConverter cellPositionToPixelConverter) {
    List<CellColor> cells = cellColorCollection.getCells();
    for(CellColor cell : cells) {
      Rectangle2D rectangle = cellPositionToPixelConverter.getBoundsForCell(cell.cellPosition());
      Color color = cell.color();
      if (color == null) {
        color = Color.DARK_GRAY;
      }
      graphics2D.setColor(color);
      graphics2D.fill(rectangle);
    }
  }
}
