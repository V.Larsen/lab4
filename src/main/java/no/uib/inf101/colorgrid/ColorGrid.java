package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{
  int rows;
  int cols;
  CellPosition pos;
  List<List<Color>> grid;
  

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    grid = new ArrayList<>();

    for(int i = 0 ; i < rows ; i++) {
      grid.add(i, new ArrayList<>());
      for(int j = 0 ; j < cols ; j++) {
        grid.get(i).add(j, null);
      }
    }
}

  @Override
  public int rows() {
    
    return this.rows;
  }

  @Override
  public int cols() {
    
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cell = new ArrayList<>();
    for(int i = 0 ; i < this.rows ; i++) {
      for(int j = 0 ; j < this.cols ; j++) {
        cell.add(new CellColor(new CellPosition(i, j), this.grid.get(i).get(j)));
      }
    } 
    return cell;
  }

  @Override
  public Color get(CellPosition pos) {
    Color color = this.grid.get(pos.row()).get(pos.col());
    return color;
  }

  @Override
  public void set(CellPosition pos, Color color) {
    this.grid.get(pos.row()).set(pos.col(), color);
    
  }
  
}
